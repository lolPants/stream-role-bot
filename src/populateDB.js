// Redis
const { getData, setData } = require('./accessDB.js')(!!process.env.USEFILES)

/**
 * @param {string[]} guilds Array of Guild IDs
 * @returns {Promise.<void>}
 */
const populateDB = guilds => {
  let tasks = guilds.map(id => fillGuild(id))
  return Promise.all(tasks)
}

/**
 * @param {string} guild Guild ID
 * @returns {Promise.<void>}
 */
const fillGuild = async guild => {
  let test = await getData(guild)
  if (test === null) {
    const template = {
      roles: [],
      channels: [],
      liveIDs: [],
      mentionEveryone: false,
    }
    return setData(guild, template)
  } else {
    return null
  }
}

module.exports = populateDB

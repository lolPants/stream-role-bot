// Database Engines
const fs = require('fs-extra')
const path = require('path')
const Redis = require('ioredis')
const redis = process.env.USEFILES ? null : new Redis(process.env.REDIS_HOST || 'redis')

const DB_FILE = path.join(__dirname, '..', 'db.json')

/**
 * @typedef {Object} GuildSettings
 * @property {string[]} roles
 * @property {string[]} channels
 * @property {string[]} liveIDs
 * @property {boolean} mentionEveryone
 */

/**
 * Get Guild Settings
 * @param {string} guild Guild ID
 * @returns {Promise.<GuildSettings>}
 */
const getDataRedis = async guild => {
  /**
   * @type {GuildSettings}
   */
  let data = await redis.get(guild)
  return JSON.parse(data)
}

/**
 * Set Guild Settings
 * @param {string} guild Guild ID
 * @param {GuildSettings} settings Settings Object
 * @returns {Promise.<void>}
 */
const setDataRedis = (guild, settings) => redis.set(guild, JSON.stringify(settings))

/**
 * Get Guild Settings
 * @param {string} guild Guild ID
 * @returns {Promise.<GuildSettings>}
 */
const getDataFile = async guild => {
  await fs.ensureFile(DB_FILE)

  let data = await fs.readJSON(DB_FILE, { throws: false })
  let json = data ? data : {}

  if (!json[guild]) json[guild] = null
  return json[guild]
}

/**
 * Set Guild Settings
 * @param {string} guild Guild ID
 * @param {GuildSettings} settings Settings Object
 * @returns {Promise.<void>}
 */
const setDataFile = async (guild, settings) => {
  await fs.ensureFile(DB_FILE)

  let data = await fs.readJSON(DB_FILE, { throws: false })
  let json = data ? data : {}

  json[guild] = settings
  return fs.writeJSON(DB_FILE, json)
}

module.exports = usefiles => {
  if (usefiles) return { getData: getDataFile, setData: setDataFile }
  else return { getData: getDataRedis, setData: setDataRedis }
}
